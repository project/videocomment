function videocomment_record(player, params) {
  params = player.getConfig();
  params.type='camera';
  player.sendEvent("LOAD", params);
  player.sendEvent('PLAY');
}

function videocomment_preview(player, params) {
  params = player.getConfig();
  params.type='video';
  player.sendEvent("LOAD",params);
  player.sendEvent('REDRAW');
  player.sendEvent('PLAY');
}

function videocomment_stop(player, params) {
  player.sendEvent('STOP');
}

function videocomment_button_callback(player, params, action) {
  eval('videocomment_' + action + '(player, params)');
}

function videocomment(obj, params) {
  var s1 = new SWFObject(videocomment_config.path + "player.swf", params.mediaplayer_object_id, "328", "200", "9", "#FFFFFF");
  s1.addParam("allowfullscreen","true");
  s1.addParam("allowscriptaccess","always");
  
  if (params.mode == 'play') {
    s1.addParam("flashvars", "type=video&file=" + params.flashvars.file);
  }
  
  if (params.mode == 'record') {
    s1.addParam("flashvars", "controlbar=none&displayclick=none&type=camera&file=" + params.flashvars.file + "&streamer=" + params.flashvars.streamer);
  }
  s1.write(obj.id);
  
  if (params.mode == 'record') {  
    player = $('#' + params.mediaplayer_object_id).get(0);
    $('.videocomment-mediaplayer-button').click(function () {videocomment_button_callback(player, params, this.id.replace(params.mediaplayer_button_base,'')); return false;});
  }
}