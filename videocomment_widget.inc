<?php
function videocomment_widget_widget_settings_form($widget) {
  $form = array();
  $form['streamer'] = array(
    '#type' => 'textfield',
    '#title' => t('Stream to record the video'),
    '#default_value' => is_string($widget['streamer']) ? $widget['streamer'] : 'rtmp://localhost/videorecording',
    '#size' => 64,
    '#description' => t('Sed luctus posuere justo.'),
    '#weight' => 8,
  );

	return $form;
}

function videocomment_widget_widget_settings_save($widget) {
  return array(
		'streamer'
  );
}

function videocomment_widget_process($element, $edit, &$form_state, $form) {
	$item = $element['#value'];
  $field_name = $element['#field_name'];
  $delta = $element['#delta'];
  $field = content_fields($element['#field_name'], $element['#type_name']);
	$path =  file_create_path($field['widget']['file_path']);

	if (!$form_state['values']['nid']) {
		// File doesn't exists

		if (!$element['#needs_validation'] && ($item['fid'] == 0)) {
			$file =  videocomment_create_file($path);
		}
		else {
			// Fetch file informations
			$file = videocomment_get_file($item['fid']);
		}		
			// Make a record widget
	
			$js_params['mode'] = 'record';
			$js_params['flashvars']['streamer'] = $field['widget']['streamer'];
			$js_params['flashvars']['type'] = 'camera';
			$js_params['flashvars']['file'] = videocomment_file_to_stream($file->filename);
	}
	else {
		$js_params['mode'] = 'play';
		$js_params['flashvars']['type'] = 'video';
		$file = videocomment_get_file($item['fid']);
		$js_params['flashvars']['file'] = file_create_url($file->filepath);
		
	}
	 	$element['fid'] = array('#type' => 'hidden', '#value' =>  $file->fid);
	// Place holder for flash player

  $element['videocomment'] = array('#value' => theme('videocomment_player', $element['#id'], $js_params));

  return $element;
}

function theme_videocomment_widget($element) {
  return theme('form_element', $element, $element['#children']);
}